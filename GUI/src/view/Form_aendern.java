package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JFormattedTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.Insets;

public class Form_aendern extends JFrame {
	

	private JPanel contentPane;
	private JTextField txtHierBitteText;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aendern frame = new Form_aendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */
	public Form_aendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 338, 557);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAufgabe = new JLabel("Aufgabe1:");
		lblAufgabe.setBounds(12, 44, 59, 16);
		contentPane.add(lblAufgabe);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonGruen_clicked();
			}
		});
		btnGrn.setBounds(12, 73, 78, 25);
		contentPane.add(btnGrn);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRot_clicked();
			}
		});
		btnRot.setBounds(102, 73, 107, 25);
		contentPane.add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlau_clicked();
			}
		});
		btnBlau.setBounds(221, 73, 87, 25);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGelb_clicked();
			}
		});
		btnGelb.setBounds(12, 111, 78, 25);
		contentPane.add(btnGelb);
		
		JButton btnStrdFarbe = new JButton("Strd. Farbe");
		btnStrdFarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStrdfarbe_clicked();
			}
		});
		btnStrdFarbe.setBounds(90, 111, 107, 25);
		contentPane.add(btnStrdFarbe);
		
		JLabel lblAufgabe_1 = new JLabel("Aufgabe2:");
		lblAufgabe_1.setBounds(15, 157, 76, 16);
		contentPane.add(lblAufgabe_1);


		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setBounds(12, 187, 78, 25);
		contentPane.add(btnArial);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(12, 221, 296, 22);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnLabel_clicked();
			}
		});
		btnInsLabelSchreiben.setBounds(12, 256, 143, 25);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnTextImLabel.setBounds(151, 256, 157, 25);
		contentPane.add(btnTextImLabel);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonFarbeWählen_clicked();
			}
		});
		btnFarbeWhlen.setBounds(198, 111, 110, 25);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblDieserTextWird = new JLabel("Dieser Text wird ge\u00E4ndert ");
		lblDieserTextWird.setBounds(90, 13, 176, 16);
		contentPane.add(lblDieserTextWird);
		

	}

	protected void btnLabel_clicked() {
		//lblDieserTextWird.setText(txtHierBitteText.getText());
		
	}

	public void buttonArial_clicked() {
//		lblBeispieltext.setFont(new Font("Arial", Font.PLAIN, 12));
		
	}

	protected void buttonStrdfarbe_clicked() {
		this.contentPane.setBackground(new Color(0xEEEEEE));
		
	}

	protected void buttonGelb_clicked() {
		this.contentPane.setBackground(Color.yellow);
		
	}

	protected void buttonBlau_clicked() {
		this.contentPane.setBackground(Color.blue);
		
	}

	protected void buttonRot_clicked() {
		this.contentPane.setBackground(Color.red);
		
	}

	protected void buttonGruen_clicked() {
		this.contentPane.setBackground(Color.green);
		
	}
	
	public void buttonFarbeWählen_clicked() {
        Color c = JColorChooser.showDialog(null, "Wähle Farbe", null);
        contentPane.setBackground(c);
    }
}








public void buttonRot_clicked() {
    this.contentPane.setBackground(Color.RED);
}

public void buttonGrün_clicked() {
    this.contentPane.setBackground(Color.GREEN);
}

public void buttonBlau_clicked() {
    this.contentPane.setBackground(Color.BLUE);
}

public void buttonGelb_clicked() {
    this.contentPane.setBackground(Color.YELLOW);
}

public void buttonStandardFarbe_clicked() {
    this.contentPane.setBackground(new Color(0xEEEEEE));
}

public void buttonFarbeWählen_clicked() {
    Color c = JColorChooser.showDialog(null, "Wähle Farbe", null);
    contentPane.setBackground(c);
}

public void buttonArial_clicked() {
    lblDieserText.setFont(new Font("Arial", Font.PLAIN,12));
}

public void buttonComicSansMS_clicked() {
    lblDieserText.setFont(new Font("Comic Sans MS", Font.PLAIN,12));
}

public void buttonCourierNew_clicked() {
    lblDieserText.setFont(new Font("Courier New", Font.PLAIN,12));
}

public void buttonInsLabelSchreiben_clicked() {
    lblDieserText.setText(txtHierBitteText.getText());
}

public void buttonTextLöschen_clicked() {
    lblDieserText.setText("");
}

public void buttonSchriftfarbeRot_clicked() {
    lblDieserText.setForeground(Color.RED);
}

public void buttonSchriftfarbeBlau_clicked() {
    lblDieserText.setForeground(Color.BLUE);
}

public void buttonSchriftfarbeSchwarz_clicked() {
    lblDieserText.setForeground(Color.BLACK);
}

public void buttonSchriftGrößer_clicked() {
    int groesse = lblDieserText.getFont().getSize();
    lblDieserText.setFont(new Font("Arial", Font.PLAIN, groesse + 1));
}

public void buttonSchriftKleiner_clicked() {
    int groesse = lblDieserText.getFont().getSize();
    lblDieserText.setFont(new Font("Arial", Font.PLAIN, groesse - 1));
}

public void buttonLinksbündig_clicked() {
    lblDieserText.setHorizontalAlignment(SwingConstants.LEFT);
}

public void buttonZentriert_clicked() {
    lblDieserText.setHorizontalAlignment(SwingConstants.CENTER);
}

public void buttonRechtsbündig_clicked() {
    lblDieserText.setHorizontalAlignment(SwingConstants.RIGHT);
}

public void buttonExit_clicked() {
    System.exit(1);
}

