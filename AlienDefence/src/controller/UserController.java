package controller;

import java.util.List;

import model.Level;
import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IPersistance alienDefenceModel) {
		this.userPersistance = alienDefenceModel.getUserPersistance();
		}
	

	public void createUser(User user) {
		userPersistance.createUser(user);
		}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	
	public User readUser(String username) {
		if(userPersistance.readUser(username) !=null) {
			return userPersistance.readUser(username);
		
	}
	else { 
		return null;
	}
	}
	
	
	public void changeUser(User user) {
		userPersistance.updateUser(user);
		}

	 public void deleteUser(User user) {
		userPersistance.deleteUser(user);
		}
	
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username);
		if(user != null) {
		return false;
		}
		
		if(user.getPassword().equals(passwort)) {
		return true;
		}
		return false;
}
}
